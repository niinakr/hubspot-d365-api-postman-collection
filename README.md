


# HubSpot-D365-api-postman-collection
Repo pitää sisällään Postmanin JSON-muotoisen collectionin, joka testaa kaksisuuntaista tiedonsiirtoa HubSpotista Dynamics 365:seen. Collection pitää sisällään API-kutsuja, testisettejä ja scriptejä, jotka kuljettavat dataa kutsusta toiseen mallintaen haluttua käyttötapausta. 

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/42c64d7ade525ce67e8f)

HubSpot-D365 Collectionin HTTP-pyyntöjen dokumentaatio löytyy täältä: [HubSpot-D365 dokumentaatio](https://documenter.getpostman.com/view/2787159/hubspot-d365-integration-v1/7EBgusw) (ei näytä testitapauksia)

Tuo repon Postman Collection [tällä ohjeella](https://www.getpostman.com/docs/postman/collections/data_formats) 

## Käytetyt ympäristömuuttujat:

Autentikaatioon liittyvät:

* hapikey: HubSpot Api key
* Client_id: Azure AD:sta
* Client_secret: Azure AD:sta
* tenant_id: Azure AD:sta
* resource:  Dynamics CRM:n Tenant URL
* Username:  Dynamics CRM
* Password:  Dynamics CRM

HTTP-kyselyihin liittyvät:

* changed_email: -
* new_lastname: -


Luodaan kyselyjen aikana automaattisesti:

* vid: visitor id (HubSpot)
* email: -
* leadid: Lead entityn primary key (D365)

## API-dokumentaatiot

* [HubSpot](https://developers.hubspot.com/docs/overview) - HubSpotin API-dokumentaatio
* [Dynamics 365](https://docs.microsoft.com/fi-fi/dynamics365/customer-engagement/web-api/about?view=dynamics-ce-odata-9) - Dynamics 365 WEB API-dokumentaatio
* [Postman](https://documenter.getpostman.com/view/2787159/hubspot-d365-integration-v1/7EBgusw) - Postman collectionin dokumentaatio


## Autentikaatio

* D365 - OAuth 2.0 
*Vaatii autentikaatioon Azuren AD:hen asennetun client-sovelluksen, joka vastaanottaa autentikaatio-tokenin. Client-sovellukselle annetaan luvat keskusteluyhteen CRM:n kanssa. Lisää autentikaation protokollasta täältä:* [Use OAuth to Authenticate with the CRM Service](https://blogs.msdn.microsoft.com/crm/2013/12/12/use-oauth-to-authenticate-with-the-crm-service/) 

* HubSpot - API key (tai OAuth 2.0)

## Newman

Newman on komentoriviajuri Postmanille. Se mahdollistaa Postman Collectionien ajon suoraan komentoriviltä, sekä automaattisen Rest Api -testauksen osana CI-ympäristöä build steppinä.  Newman tarvii toimiakseen NodeJS:n (>=v4.)

```
$ npm install newman --global;
```

Newman voi ajaa collectioneita joko exportattu json-tiedosto tai URL-linkki parametrinä. Ympäristömuuttujat tuodaan ajoon optiolla  -e 
```
$ newman -c mycollection.json
```
```
$ newman -u https://www.getpostman.com/collections/cb208e7e64056f5294e5 -e devenvironment.json
```
Newman tuo testitulokset komentoriville tai halutessaan myös muille formaateille (hmtl, json, junit) optiolla -r

```
$ newman -c mycollection.json  -r html
```

Esimerkkikuva komentotulkin testitulosten statuksista:

![alt text](https://github.devcloud.elisa.fi/niinarantanen/HubSpot-D365-api-postman-collection/blob/master/newmancli.PNG)


## Postman pro

* Tarjoaa oman versionhallinnan "Team Sharing", jonka kautta voi jakaa collectioneita ja ympäristöjä
* Mahdollistaa API dokumentaation julkaisun web-sivuna
* Postman pro API [Postman Pro API documentation](https://docs.api.getpostman.com/?_ga=2.171294560.1916297422.1511343812-1596134276.1510662191)
